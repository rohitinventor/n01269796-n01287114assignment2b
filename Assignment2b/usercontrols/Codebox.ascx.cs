﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;

namespace Assignment2b.usercontrols
{

    public partial class Codebox : System.Web.UI.UserControl
    {

        public string code
        {
            get { return (string)ViewState["code"]; }
            set { ViewState["code"] = value; }

        }
        public string owner
        {
            get { return (string)ViewState["owner"]; }
            set { ViewState["owner"] = value; }

        }
            //refrence from professor code
        DataView CreateCodeSource()
        {
            DataTable codedata = new DataTable();


            DataColumn idx_col = new DataColumn();

            DataColumn code_col = new DataColumn();

            DataRow coderow;

            idx_col.ColumnName = "Line";
            idx_col.DataType = System.Type.GetType("System.Int32");

            codedata.Columns.Add(idx_col);

            code_col.ColumnName = "Code";
            code_col.DataType = System.Type.GetType("System.String");
            codedata.Columns.Add(code_col);


            List<string> mycode;

            if (code == "My_Javascript"&& owner=="Me")
            {


                mycode = new List<string>(new string[]{
                "document.getElementById(\"result\").innerHTML = \"Array : \"+fruits;",
                "function sortArray(){",
                "fruits.sort();",
                "document.getElementById(\"result\").innerHTML = \"Array : \"+fruits;",
                "}",
                "function reverseArray(){",
                "fruits.reverse();",
                "document.getElementById(\"result\").innerHTML = \"Array : \"+fruits;",
                    " }"
                });
            }



            else if (code == "Teacher_Javascript" && owner == "Teacher")
            {
                mycode = new List<string>(new string[]{
                "function add(){",
                "var price1 = 5;",
                "var price2 = 6;",
                "var total = price1 + price2;",
                "document.getElementById(\"resultTotal\").innerHTML = \"Result : \"+total;",
                "}",
                });
            }




            else if (code == "My_Html" && owner == "Me")
            {

                mycode = new List<string>(new string[]{
               "<form id = \"submitForm\">",
                "<fieldset>",
                "<label for= \"fname\" ></ label >",
                "<input type = \"text\" id = \"fName\">",
                 "<label for= \"lname\" ></ label > ",
                "<input type = \"text\" id = \"lName\">",
                "<input type = \"submit\" id = \"submitbutton\" value = \"Submit\">",
                "</fieldset>",
                  "</form> "
                });


            }






            else if (code == "Teacher_Html" && owner == "Teacher")
            {
                mycode = new List<string>(new string[]{
                    "<body style = \"background-color:powderblue;\">",
                "<h1> This is a heading </h1>",
                "<p>This is a paragraph.</p>",
                "</body>",
                 
                });
            }


            else if (code == "My_SQL" && owner == "Me")
            {

                mycode = new List<string>(new string[]{
                "SELECT *",
                "FROM INVOICES",
              
                });


            }


            else if (code == "Teacher_SQL" && owner == "Teacher")
            {

                //SELECT LastName, FirstName, Address 
                   
                mycode = new List<string>(new string[]{
                "SELECT LastName, FirstName, Address",
                "FROM Persons",
                "WHERE Address IS NULL"
                });
            }



            else
            {
                mycode = new List<string>();

            }
            int i = 0;
            foreach (string code_line in mycode)
            {
                coderow = codedata.NewRow();
                coderow[idx_col.ColumnName] = i;
                string formatted_code = System.Net.WebUtility.HtmlEncode(code_line);
                formatted_code = formatted_code.Replace("~", "&nbsp;&nbsp;&nbsp;");
                coderow[code_col.ColumnName] = formatted_code;

                i++;
                codedata.Rows.Add(coderow);
            }


            DataView codeview = new DataView(codedata);
            return codeview;
        }






        protected void Page_Load(object sender, EventArgs e)
        {
            DataView ds = CreateCodeSource();

            mycodebox.DataSource = ds;



            mycodebox.DataBind();

            



        }

    }
}
